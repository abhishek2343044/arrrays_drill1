function map(elements, cb) {
  let NewArray = [];
  for (index = 0; index < elements.length; index++) {
    NewArray.push(cb(elements[index]));
  }
  return NewArray;
}

module.exports = map;
