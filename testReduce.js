const reduce = require('./reduce.js');

const items = [1, 2, 3, 4, 5, 5];

var sum = reduce(
  items,
  (starting, current) => {
    return (starting += current);
  },
  0
);

console.log(sum);
