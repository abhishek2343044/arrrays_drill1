function reduce(elements, cb, startingValue = elements[0]) {
  for (index = 0; index < elements.length; index++) {
    startingValue = cb(startingValue, elements[index]);
  }

  return startingValue;
}

module.exports = reduce;
