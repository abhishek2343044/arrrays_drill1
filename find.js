function find(elements, cb) {
  let element;
  for (index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      return elements[index];
    }
  }
  return element;
}

module.exports = find;
