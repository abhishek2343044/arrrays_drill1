function each(elements, cb) {
  for (index = 0; index < elements.length; index++) {
    cb(elements[index], index);
  }
}

module.exports = each;
