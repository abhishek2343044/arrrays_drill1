function filter(elements, cb) {
  let NewArray = [];
  for (index = 0; index < elements.length; index++) {
    if (cb(elements[index])) {
      NewArray.push(elements[index]);
    }
  }
  return NewArray;
}

module.exports = filter;
