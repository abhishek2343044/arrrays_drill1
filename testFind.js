const find = require('./find.js');

const items = [1, 2, 3, 4, 5, 5];

var element = find(items, (data) => {
  return data % 2 == 0;
});

console.log(element);
