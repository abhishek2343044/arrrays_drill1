function flatten(elements) {
  let newArray = [];

  function doFlat(element) {
    for (let index = 0; index < element.length; index++) {
      if (!Array.isArray(element[index])) {
        newArray.push(element[index]);
      } else {
        doFlat(element[index]);
      }
    }
  }

  doFlat(elements);
  return newArray;
}

module.exports = flatten;
